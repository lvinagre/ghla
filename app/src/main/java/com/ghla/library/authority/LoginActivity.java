package com.ghla.library.authority;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.ghla.library.authority.model.User;
import com.ghla.library.authority.utils.CustomMessage;
import com.ghla.library.authority.utils.SharedPrefs;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import static com.ghla.library.authority.utils.Network.isNetworkAvailable;


/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {

    // UI references.
    private EditText mEmailView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;

    // Firebase auth reference
    private FirebaseAuth mAuth;

    // Activity referencee
    private Activity loginActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Set up the login form.
        mEmailView = (EditText) findViewById(R.id.username);
        mPasswordView = (EditText) findViewById(R.id.password);
        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);

        // Instantiate firebase
        mAuth = FirebaseAuth.getInstance();

        // Get reference to activity
        loginActivity = this;
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser user = mAuth.getCurrentUser();

        if (user != null) {
            showProgress(true);
            if (isNetworkAvailable(this)) {
                retrieveUserProfile(user.getUid(), userProfileCb);
            } else {
                loadBaseActivity();
            }
        }
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    public void attemptLogin(View view) {
        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(password)) {
            mPasswordView.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            cancel = true;
        } else if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            if (isNetworkAvailable(this)) {
                showProgress(true);
                doLogin(email, password, view);
            } else {
                CustomMessage.getCustomSnackbar(view,R.string.error_no_network, Snackbar.LENGTH_SHORT ).show();
            }
        }
    }

    private boolean isEmailValid(String email) {
        return email.matches(".+@.+(\\.[a-zA-Z]{2,3}){1,2}");
    }

    private boolean isPasswordValid(String password) {
        // 6 is min password for Firebase
        return password.length() > 6;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    // Handle user authentication with Firebase
    private void doLogin(String email, String password, final View view) {
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Get user info
                            // Sign in success
                            retrieveUserProfile(mAuth.getCurrentUser().getUid(), userProfileCb);
                        } else {
                            // If sign in fails, display a message to the user.
                            showProgress(false);
                            CustomMessage.getCustomSnackbar(view,R.string.error_auth_failed, Snackbar.LENGTH_SHORT ).show();
                            mPasswordView.requestFocus();
                        }
                    }
                });
    }

    private void retrieveUserProfile(String uid, ValueEventListener onComplete) {
        App.mDatabase.child(getString(R.string.FIREBASE_USERS)).child(uid).addListenerForSingleValueEvent(onComplete);
    }

    private ValueEventListener userProfileCb = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            if (dataSnapshot.exists()) {
                User tmpUser = dataSnapshot.getValue(User.class);

                if (!tmpUser.getName().isEmpty() && !tmpUser.getRole().isEmpty() && tmpUser.getLibraries() != null) {
                    SharedPrefs.saveUser(loginActivity, tmpUser);
                    loadBaseActivity();
                } else {
                    mAuth.signOut();
                    SharedPrefs.removeUser(loginActivity);
                    showProgress(false);
                }
            } else {
                mAuth.signOut();
                SharedPrefs.removeUser(loginActivity);
                showProgress(false);
            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
            mAuth.signOut();
            SharedPrefs.removeUser(loginActivity);
            showProgress(false);
        }
    };

    private void loadBaseActivity() {
        Intent intent = new Intent(this, BaseActivity.class);
        startActivity(intent);
        finish();
    }
}

