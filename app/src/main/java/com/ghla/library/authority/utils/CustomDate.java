package com.ghla.library.authority.utils;

import android.content.Context;

import com.ghla.library.authority.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class CustomDate {

    public enum CustomDateFormat{
        Daily, Weekly, Quarterly, Monthly
    }

    public static String getCurrentDate(TimeZone timezone, CustomDateFormat customDateFormat) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(timezone);

        switch (customDateFormat) {
            case Daily:
                return new SimpleDateFormat("yyyyMMdd").format(calendar.getTime());
            case Weekly:
                return new SimpleDateFormat("yyyy_ww").format(calendar.getTime());
            case Monthly:
                return new SimpleDateFormat("yyyyMM").format(calendar.getTime());
            case Quarterly:
                int month = calendar.get(Calendar.MONTH);
                if (month >= Calendar.JANUARY && month <= Calendar.MARCH) {
                    return calendar.get(Calendar.YEAR) + "_Q1";
                } else if (month >= Calendar.APRIL && month <= Calendar.JUNE) {
                    return calendar.get(Calendar.YEAR) + "_Q2";
                } else if (month >= Calendar.JULY && month <= Calendar.SEPTEMBER) {
                    return calendar.get(Calendar.YEAR) + "_Q3";
                } else {
                    return calendar.get(Calendar.YEAR) + "_Q4";
                }
            default:
                return new SimpleDateFormat("yyyyMMdd").format(calendar.getTime());
        }
    }

    public static String getDateInCustom(TimeZone timezone, String date, CustomDateFormat customDateFormat) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Integer.parseInt(date.substring(0, 4)), Integer.parseInt(date.substring(4, 6)) - 1, Integer.parseInt(date.substring(6, 8)));
        calendar.setTimeZone(timezone);

        switch (customDateFormat) {
            case Daily:
                return new SimpleDateFormat("yyyyMMdd").format(calendar.getTime());
            case Weekly:
                return new SimpleDateFormat("yyyy_ww").format(calendar.getTime());
            case Monthly:
                return new SimpleDateFormat("yyyyMM").format(calendar.getTime());
            case Quarterly:
                int month = calendar.get(Calendar.MONTH);
                if (month >= Calendar.JANUARY && month <= Calendar.MARCH) {
                    return calendar.get(Calendar.YEAR) + "_Q1";
                } else if (month >= Calendar.APRIL && month <= Calendar.JUNE) {
                    return calendar.get(Calendar.YEAR) + "_Q2";
                } else if (month >= Calendar.JULY && month <= Calendar.SEPTEMBER) {
                    return calendar.get(Calendar.YEAR) + "_Q3";
                } else {
                    return calendar.get(Calendar.YEAR) + "_Q4";
                }
            default:
                return new SimpleDateFormat("yyyyMMdd").format(calendar.getTime());
        }
    }

    public static String convertCustomToHuman(Context context, String date, CustomDateFormat customDateFormat) throws ParseException {
        String customDate;
        SimpleDateFormat defaultFormat, human;
        Date finalDate;

        switch (customDateFormat) {
            case Daily:
                defaultFormat = new SimpleDateFormat("yyyyMMdd");
                human = new SimpleDateFormat("dd MMMM, yyyy");
                finalDate = defaultFormat.parse(date);
                return human.format(finalDate);
            case Weekly:
                customDate = context.getString(R.string.week, date.split("_")[1]) + " " +  date.split("_")[0];
                return customDate;
            case Monthly:
                defaultFormat = new SimpleDateFormat("yyyyMM");
                human = new SimpleDateFormat("MMMM/yyyy");
                finalDate = defaultFormat.parse(date);
                return human.format(finalDate);
            case Quarterly:
                switch (date.split("_Q")[1]) {
                    case "1":
                        customDate = context.getString(R.string.quarter_first);
                        break;
                    case "2":
                        customDate = context.getString(R.string.quarter_second);
                        break;
                    case "3":
                        customDate = context.getString(R.string.quarter_third);
                        break;
                    case "4":
                        customDate = context.getString(R.string.quarter_fourth);
                        break;
                    default:
                        customDate = "";
                }

                customDate += " " + date.split("_Q")[0];
                return customDate;
            default:
                return date;
        }
    }
}
