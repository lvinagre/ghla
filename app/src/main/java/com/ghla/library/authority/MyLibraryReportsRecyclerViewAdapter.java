package com.ghla.library.authority;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ghla.library.authority.LibraryReportsFragment.OnListFragmentInteractionListener;
import com.ghla.library.authority.model.Report;
import com.ghla.library.authority.utils.CustomDate;

import java.text.ParseException;
import java.util.List;

public class MyLibraryReportsRecyclerViewAdapter extends RecyclerView.Adapter<MyLibraryReportsRecyclerViewAdapter.ViewHolder> {

    private final String libraryName;
    private final List<Report> mReports;
    private final OnListFragmentInteractionListener mListener;

    public MyLibraryReportsRecyclerViewAdapter(String libraryName, List<Report> reports, OnListFragmentInteractionListener listener) {
        this.libraryName = libraryName;
        mReports = reports;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_library_reports, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mReport = mReports.get(position);
        holder.mReportMetric.setText(mReports.get(position).getMetric().getMetricTypeString());

        if (holder.mReport.getMetric().getMetricTypeString().matches(".*Daily.*")) {
            try {
                holder.mReportDate.setText(CustomDate.convertCustomToHuman(holder.itemView.getContext(), mReports.get(position).getDate(), CustomDate.CustomDateFormat.Daily));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else if (holder.mReport.getMetric().getMetricTypeString().matches(".*Weekly.*")) {
            try {
                holder.mReportDate.setText(CustomDate.convertCustomToHuman(holder.itemView.getContext(), mReports.get(position).getDate(), CustomDate.CustomDateFormat.Weekly));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else if (holder.mReport.getMetric().getMetricTypeString().matches(".*Monthly.*")) {
            try {
                holder.mReportDate.setText(CustomDate.convertCustomToHuman(holder.itemView.getContext(), mReports.get(position).getDate(), CustomDate.CustomDateFormat.Monthly));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else if (holder.mReport.getMetric().getMetricTypeString().matches(".*Quarterly.*")) {
            try {
                holder.mReportDate.setText(CustomDate.convertCustomToHuman(holder.itemView.getContext(), mReports.get(position).getDate(), CustomDate.CustomDateFormat.Quarterly));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onLibraryReportListFragmentInteraction(libraryName, holder.mReport);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mReports.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public Report mReport;
        public final TextView mReportMetric, mReportDate;

        public ViewHolder(View view) {
            super(view);
            mReportMetric = (TextView) view.findViewById(R.id.library_report_metric);
            mReportDate = (TextView) view.findViewById(R.id.library_report_date);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mReportMetric.getText() + " (" + mReportDate + ")'";
        }
    }
}
