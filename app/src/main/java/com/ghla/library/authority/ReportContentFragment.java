package com.ghla.library.authority;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ghla.library.authority.model.Report;
import com.ghla.library.authority.utils.SharedPrefs;

public class ReportContentFragment extends Fragment {

    private int mColumnCount = 1;
    private static final String REPORT = "REPORT";
    private static final String LIBRARY_NAME = "LIBRARY_NAME";
    private Report mReport = null;
    private String mLibraryName = null;

    ReportContentFragment reportContentFragment = this;
    private Fragment mFragment;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ReportContentFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mReport = (Report) getArguments().get(REPORT);
        mLibraryName = (String) getArguments().get(LIBRARY_NAME);
    }

    public static ReportContentFragment newInstance(Report report, String libraryName) {
        ReportContentFragment fragment = new ReportContentFragment();
        Bundle args = new Bundle();
        args.putParcelable(REPORT, report);
        args.putString(LIBRARY_NAME, libraryName);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reportcontent_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            final RecyclerView recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            recyclerView.setAdapter(new MyReportContentRecyclerViewAdapter(mReport, mLibraryName, recyclerView, reportContentFragment));
        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void dismissFragment(){
        String fragmentTag;
        Fragment fragment;

        if (reportContentFragment.getContext() != null) {
            if (SharedPrefs.getUser(reportContentFragment.getActivity()).getRole().equals("M")) {
                fragmentTag = getString(R.string.FRAGMENT_REPORTS);
                fragment = new ReportsFragment();
            } else {
                fragmentTag = getString(R.string.FRAGMENT_LIBRARIES);
                fragment = new LibraryFragment();
            }

            mFragment = mFragment == null ? fragment : mFragment;
            FragmentManager fragmentManager = getFragmentManager();
            if (fragmentManager != null) {
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(reportContentFragment.getId(), mFragment, fragmentTag);
                fragmentTransaction.commit();
            }
        }
    }
}
