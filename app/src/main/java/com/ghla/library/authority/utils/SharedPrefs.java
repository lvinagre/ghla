package com.ghla.library.authority.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.ghla.library.authority.R;
import com.ghla.library.authority.model.User;

public class SharedPrefs {

    public static void saveUser(Activity activity, User user) {
        Context context = activity.getApplicationContext();
        SharedPreferences sp = activity.getSharedPreferences(context.getString(R.string.FIREBASE_USERS), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        String userJson = new JSONHelper().serializeJSON(user);
        editor.putString(context.getString(R.string.FIREBASE_USER), userJson);
        editor.apply();
    }

    public static void removeUser(Activity activity) {
        Context context = activity.getApplicationContext();
        SharedPreferences sp = activity.getSharedPreferences(context.getString(R.string.FIREBASE_USERS), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.remove(context.getString(R.string.FIREBASE_USER));
        editor.apply();
    }

    public static User getUser(Activity activity) {
        Context context = activity.getApplicationContext();
        SharedPreferences sp = activity.getSharedPreferences(context.getString(R.string.FIREBASE_USERS), Context.MODE_PRIVATE);
        String userJson = sp.getString(context.getString(R.string.FIREBASE_USER), null);
        User user = (User) new JSONHelper().deserializeJSON(userJson, User.class);
        return user;
    }
}
