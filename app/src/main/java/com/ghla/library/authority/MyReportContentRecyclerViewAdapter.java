package com.ghla.library.authority;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ghla.library.authority.model.Answerable;
import com.ghla.library.authority.model.Question;
import com.ghla.library.authority.model.Report;
import com.ghla.library.authority.model.Subtitle;
import com.ghla.library.authority.model.Title;
import com.ghla.library.authority.utils.CustomDate;
import com.ghla.library.authority.utils.CustomMessage;
import com.ghla.library.authority.utils.Network;
import com.ghla.library.authority.utils.SharedPrefs;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.util.List;
import java.util.TimeZone;

public class MyReportContentRecyclerViewAdapter extends RecyclerView.Adapter<MyReportContentRecyclerViewAdapter.ViewHolder> {

    private Report mReport;
    private String mLibraryName;
    private final List<Title> mTitles;
    private Button mSubmitButton, mEditButton, mApproveButton, mChangeRequestButton;
    private TextView mStatusTextView;
    private RecyclerView rootLayout;
    private ReportContentFragment m_ReportsFragment;


    public MyReportContentRecyclerViewAdapter(Report report, String libraryName, RecyclerView recyclerView, ReportContentFragment fragment) {
        mReport = report;
        mLibraryName = libraryName;
        mTitles = report.getReport();
        rootLayout = recyclerView;
        m_ReportsFragment = fragment;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_reportcontent, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Title title = mTitles.get(position);
        LinearLayout linearLayout = (LinearLayout) holder.mView;
        addTitleView(linearLayout, title);
        //Dynamically add the subtitle and question views.
        if (title.getSubtitles() != null){
            for (Subtitle subtitle : title.getSubtitles()) {
                addSubtitleView(linearLayout, subtitle);
                if (subtitle.getQuestions() != null) {
                    for (Question question : subtitle.getQuestions()) {
                        addQuestionView(linearLayout, question);
                    }
                }
            }
        }

        if(position == mTitles.size()-1){
            addButtonsToLastTitle(linearLayout);

            // Set current report status
            switch (mReport.getStatus()) {
                case NotStarted:
                    mStatusTextView.setText(R.string.report_status_notStarted);
                    break;
                case InProgress:
                    mStatusTextView.setText(R.string.report_status_inProgress);
                    break;
                case Submitted:
                    mStatusTextView.setText(R.string.report_status_submitted);
                    break;
                case Approved:
                    mStatusTextView.setText(R.string.report_status_approved);
                    break;
                case ChangeRequested:
                    mStatusTextView.setText(R.string.report_status_change);
                    break;
                default:
                   mStatusTextView.setText(R.string.report_status_notStarted);
            }

            if (SharedPrefs.getUser(m_ReportsFragment.getActivity()).getRole().equals("M")) {
                if (mReport.getStatus() != Report.Status.Approved) {
                    if (mReport.getStatus() != Report.Status.Submitted && mReport.getStatus() != Report.Status.ChangeRequested) {
                        changeReportButtons(true);
                        changeEditTexts(rootLayout, true);
                    } else {
                        changeReportButtons(false);
                        changeEditTexts(rootLayout, false);
                    }
                } else {
                    changeEditTexts(rootLayout, false);
                }
            } else if (SharedPrefs.getUser(m_ReportsFragment.getActivity()).getRole().equals("RM")) {
                changeEditTexts(rootLayout, false);
                mApproveButton.setVisibility(View.VISIBLE);
                mChangeRequestButton.setVisibility(View.VISIBLE);
            }
        }

        holder.setIsRecyclable(false);
    }

    void addTitleView (LinearLayout layout, final Title title) {
        LayoutInflater vi = (LayoutInflater) App.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View titleLayout = vi.inflate(R.layout.report_content_title, null);
        TextView tv = titleLayout.findViewById(R.id.text);
        tv.setText(title.getText());
        addEditText(titleLayout, title);
        layout.addView(titleLayout);
    }

    void addSubtitleView (LinearLayout layout, Subtitle subtitle) {
        LayoutInflater vi = (LayoutInflater) App.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View subtitleLayout = vi.inflate(R.layout.report_content_subtitle, null);
        TextView tv = subtitleLayout.findViewById(R.id.text);
        tv.setText(subtitle.getText());
        addEditText(subtitleLayout, subtitle);
        layout.addView(subtitleLayout);
    }
    void addQuestionView (LinearLayout layout, Question question) {
        LayoutInflater vi = (LayoutInflater) App.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View questionLayout = vi.inflate(R.layout.report_content_question, null);
        TextView tv = questionLayout.findViewById(R.id.text);
        tv.setText(question.getText());
        addEditText(questionLayout, question);
        layout.addView(questionLayout);
    }


    void addEditText(View layout, final Answerable answerable){
        //If the field contains answerable true, only then add the EditText field.
        if (answerable.getAnswerable() == null) return;
        if(!answerable.getAnswerable()) return;
        EditText answerEdit = layout.findViewById(R.id.answerEdit);
        answerEdit.setVisibility(View.VISIBLE);
        if (answerable.getAnswer() != null)
            answerEdit.setText(Integer.toString(answerable.getAnswer()));
        answerEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().equals(""))
                    answerable.setAnswer(Integer.parseInt(editable.toString()));
            }
        });

        if (mReport.getStatus() != Report.Status.NotStarted && mReport.getStatus() != Report.Status.InProgress)
            answerEdit.setEnabled(false);
    }

    void addButtonsToLastTitle(LinearLayout layout) {
        LayoutInflater vi = (LayoutInflater) App.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View buttonLayout = vi.inflate(R.layout.report_content_submit, null);
        TextView textView = buttonLayout.findViewById(R.id.status_text);
        mStatusTextView = textView;
        mEditButton = buttonLayout.findViewById(R.id.edit_button);
        mEditButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mReport.setStatus(Report.Status.InProgress);
                mStatusTextView.setText(R.string.report_status_inProgress);
                changeReportButtons(true);
                changeEditTexts(rootLayout, true);
            }
        });
        mSubmitButton = buttonLayout.findViewById(R.id.submit_button);
        mSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                // Add 0 on answerables on submit
                // if they are null
                for (Title tlt : mTitles){
                    if (tlt.getAnswerable() != null)
                        if (tlt.getAnswerable() && tlt.getAnswer() == null)
                            tlt.setAnswer(0);

                    if (tlt.getSubtitles() != null)
                        for (Subtitle sbt : tlt.getSubtitles()) {
                            if (sbt.getAnswerable() != null)
                                if (sbt.getAnswerable() && sbt.getAnswer() == null)
                                    sbt.setAnswer(0);

                            if (sbt.getQuestions() != null)
                                for (Question qtn : sbt.getQuestions()) {
                                    if (qtn.getAnswerable() != null)
                                        if (qtn.getAnswerable() && qtn.getAnswer() == null)
                                            qtn.setAnswer(0);
                                }
                        }
                }

                // Get the date by Metric type
                String tmpDate = null;

                if (mReport.getMetric().getMetricTypeString().matches(".*Daily.*")) {
                    tmpDate = CustomDate.getCurrentDate(TimeZone.getTimeZone(v.getContext().getString(R.string.default_timezone)), CustomDate.CustomDateFormat.Daily);
                } else if (mReport.getMetric().getMetricTypeString().matches(".*Weekly.*")) {
                    tmpDate = CustomDate.getCurrentDate(TimeZone.getTimeZone(v.getContext().getString(R.string.default_timezone)), CustomDate.CustomDateFormat.Weekly);
                } else if (mReport.getMetric().getMetricTypeString().matches(".*Monthly.*")) {
                    tmpDate = CustomDate.getCurrentDate(TimeZone.getTimeZone(v.getContext().getString(R.string.default_timezone)), CustomDate.CustomDateFormat.Monthly);
                } else if (mReport.getMetric().getMetricTypeString().matches(".*Quarterly.*")) {
                    tmpDate = CustomDate.getCurrentDate(TimeZone.getTimeZone(v.getContext().getString(R.string.default_timezone)), CustomDate.CustomDateFormat.Quarterly);
                }

                // Prepare the report to be saved/submitted
                mReport.setReport(mTitles);
                mReport.setStatus(Report.Status.Submitted);
                changeButtonStatus(mSubmitButton, false, v.getContext().getString(R.string.report_submitting));

                // Submit the report
                App.mDatabase.child(v.getContext().getString(R.string.FIREBASE_REPORTS)).child(mLibraryName).child(mReport.getMetric().getMetricTypeString()).child(tmpDate).setValue(mReport).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            m_ReportsFragment.dismissFragment();
                            Toast.makeText(v.getContext(), v.getContext().getString(R.string.report_submitted, mReport.getMetric().getMetricTypeString()), Toast.LENGTH_LONG).show();
                        } else {
                            mReport.setStatus(Report.Status.InProgress);
                            mStatusTextView.setText(R.string.report_status_inProgress);
                            CustomMessage.getCustomSnackbar(v,v.getContext().getString(R.string.generic_error) + " " + task.getException().getMessage(), Snackbar.LENGTH_LONG ).show();
                        }
                        changeButtonStatus(mSubmitButton, true, v.getContext().getString(R.string.report_submit));
                    }
                });

                if (!Network.isNetworkAvailable(v.getContext()))
                    CustomMessage.getCustomSnackbar(v,v.getContext().getString(R.string.report_submit_no_network), Snackbar.LENGTH_LONG ).show();
            }
        });

        mApproveButton = buttonLayout.findViewById(R.id.approve_button);
        mApproveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(m_ReportsFragment.getActivity());

                builder.setTitle(R.string.report_approve_dialog_title)
                        .setMessage(R.string.report_approve_dialog_message);

                builder.setPositiveButton(R.string.report_approve_dialog_confirm, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mReport.setStatus(Report.Status.Approved);

                        App.mDatabase.child(v.getContext().getString(R.string.FIREBASE_REPORTS)).child(mLibraryName).child(mReport.getMetric().getMetricTypeString()).child(mReport.getDate()).child(v.getContext().getString(R.string.FIREBASE_REPORT_STATUS)).setValue(mReport.getStatus()).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    m_ReportsFragment.dismissFragment();
                                    Toast.makeText(v.getContext(), v.getContext().getString(R.string.report_approved, mLibraryName, mReport.getMetric().getMetricTypeString()), Toast.LENGTH_LONG).show();
                                } else {
                                    mReport.setStatus(Report.Status.Submitted);
                                    mStatusTextView.setText(R.string.report_status_submitted);
                                    CustomMessage.getCustomSnackbar(v,v.getContext().getString(R.string.generic_error) + " " + task.getException().getMessage(), Snackbar.LENGTH_LONG ).show();
                                }
                            }
                        });

                       if (!Network.isNetworkAvailable(v.getContext())) {
                           CustomMessage.getCustomSnackbar(v, v.getContext().getString(R.string.report_submit_no_network), Snackbar.LENGTH_LONG).show();
                           m_ReportsFragment.dismissFragment();
                       }
                    }
                });

                builder.setNegativeButton(R.string.report_approve_dialog_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

                builder.create().show();
            }
        });

        mChangeRequestButton = buttonLayout.findViewById(R.id.change_request_button);
        mChangeRequestButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(m_ReportsFragment.getActivity());

                builder.setTitle(R.string.report_change_request_dialog_title)
                        .setMessage(R.string.report_change_request_dialog_message);

                builder.setPositiveButton(R.string.report_change_request_dialog_confirm, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mReport.setStatus(Report.Status.ChangeRequested);

                        App.mDatabase.child(v.getContext().getString(R.string.FIREBASE_REPORTS)).child(mLibraryName).child(mReport.getMetric().getMetricTypeString()).child(mReport.getDate()).child(v.getContext().getString(R.string.FIREBASE_REPORT_STATUS)).setValue(mReport.getStatus()).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    m_ReportsFragment.dismissFragment();
                                    Toast.makeText(v.getContext(), v.getContext().getString(R.string.report_change_requested, mLibraryName, mReport.getMetric().getMetricTypeString()), Toast.LENGTH_LONG).show();
                                } else {
                                    mReport.setStatus(Report.Status.Submitted);
                                    mStatusTextView.setText(R.string.report_status_submitted);
                                    CustomMessage.getCustomSnackbar(v,v.getContext().getString(R.string.generic_error) + " " + task.getException().getMessage(), Snackbar.LENGTH_LONG ).show();
                                }
                            }
                        });

                        if (!Network.isNetworkAvailable(v.getContext())) {
                            CustomMessage.getCustomSnackbar(v, v.getContext().getString(R.string.report_submit_no_network), Snackbar.LENGTH_LONG).show();
                            m_ReportsFragment.dismissFragment();
                        }
                    }
                });

                builder.setNegativeButton(R.string.report_change_request_dialog_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

                builder.create().show();
            }
        });

        layout.addView(buttonLayout);
    }

    void changeReportButtons (boolean showSubmit) {
        if (showSubmit) {
            mEditButton.setVisibility(View.GONE);
            mSubmitButton.setVisibility(View.VISIBLE);
        } else {
            mSubmitButton.setVisibility(View.GONE);
            mEditButton.setVisibility(View.VISIBLE);
        }
    }

    void changeButtonStatus(Button button, boolean isClickable, @Nullable String text) {
        button.setClickable(isClickable);
        if (text != null && text != "") {
            button.setText(text);
        }
    }

    void changeEditTexts(ViewGroup layout, boolean enable) {
        for (int i = 0; i < layout.getChildCount(); i++) {
            if (layout.getChildAt(i) instanceof EditText) {
                layout.getChildAt(i).setEnabled(enable);
            } else if (layout.getChildAt(i) instanceof ViewGroup) {
                changeEditTexts((LinearLayout) layout.getChildAt(i), enable);
            }
        }
    }

    @Override
    public int getItemCount() {
        return mTitles.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public View mView;
        public Title title;

        public ViewHolder(View view) {
            super(view);
            mView = view;
        }
    }
}
