package com.ghla.library.authority.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

// Every title has an array of subtitles
public class Title implements Answerable, Parcelable{
    private String text;
    private Integer answer;
    private Boolean answerable = false;
    private List<Subtitle> subtitles;

    Title() {}

    Title (String text){
        this.text = text;
    }

    Title (String text, Boolean answerable){
        this.text = text;
        this.answerable = answerable;
    }

    protected Title(Parcel in) {
        text = in.readString();
        answer = in.readInt();
        answerable = in.readByte() != 0;
        subtitles = in.createTypedArrayList(Subtitle.CREATOR);
    }

    public static final Creator<Title> CREATOR = new Creator<Title>() {
        @Override
        public Title createFromParcel(Parcel in) {
            return new Title(in);
        }

        @Override
        public Title[] newArray(int size) {
            return new Title[size];
        }
    };

    void add(Subtitle subtitle) {
        if(this.subtitles == null){
            this.subtitles = new ArrayList<>();
        }
        this.subtitles.add(subtitle);
    }

    @Override
    public Boolean getAnswerable() {
        return answerable;
    }

    @Override
    public void setAnswerable(Boolean answerable) {
        this.answerable = answerable;
    }

    @Override
    public Integer getAnswer() {
        return answer;
    }

    @Override
    public void setAnswer(Integer answer) {
        this.answer = answer;
    }

    public String getText() {
        return text;
    }

    public void setText() { this.text = text; }

    public List<Subtitle> getSubtitles() {
        return subtitles;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.text);
        if (this.answer != null)
            parcel.writeInt(this.answer);
        if (this.answerable != null)
            parcel.writeByte((byte) (this.answerable ? 1 : 0));
        parcel.writeTypedList(this.subtitles);
    }
}
