package com.ghla.library.authority;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.Toast;

import com.ghla.library.authority.model.Library;
import com.ghla.library.authority.model.Report;
import com.ghla.library.authority.model.ReportType;
import com.ghla.library.authority.model.User;
import com.ghla.library.authority.utils.CustomDate;
import com.ghla.library.authority.utils.Network;
import com.ghla.library.authority.utils.SharedPrefs;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.TimeZone;

public class BaseActivity extends AppCompatActivity implements
        HomeFragment.OnFragmentInteractionListener,
        LibraryFragment.OnListFragmentInteractionListener,
        LibraryReportsFragment.OnListFragmentInteractionListener,
        ReportsCardViewAdapter.OnItemClickListener {


    private HomeFragment m_homeFragment;
    private ReportsFragment m_reportsFragment;
    private ProfileFragment m_profileFragment;
    private LibraryFragment m_libraryFragment;
    private DashboardFragment m_dashboardFragment;
    private Fragment m_currentFragment;
    private User tmpUser;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

            switch (item.getItemId()) {
                case R.id.navigation_home:
                    switchToFragment(m_homeFragment, getString(R.string.FRAGMENT_HOME));
                    return true;
                case R.id.navigation_report:
                    if (tmpUser.getRole().equals("M")) {
                        switchToFragment(m_reportsFragment, getString(R.string.FRAGMENT_REPORTS));
                    } else if (tmpUser.getRole().equals("RM")) {
                        switchToFragment(m_libraryFragment, getString(R.string.FRAGMENT_LIBRARIES));
                    }
                    return true;
                case R.id.navigation_dashboard:
                    switchToFragment(m_dashboardFragment, getString(R.string.FRAGMENT_DASHBOARD));
                    return true;
                case R.id.navigation_profile:
                    switchToFragment(m_profileFragment, getString(R.string.FRAGMENT_PROFILE));
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);

        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        m_homeFragment = new HomeFragment();
        m_reportsFragment = new ReportsFragment();
        m_profileFragment = new ProfileFragment();
        m_libraryFragment = new LibraryFragment();
        m_dashboardFragment = new DashboardFragment();


        tmpUser = SharedPrefs.getUser(this);

        if (tmpUser.getRole().equals("M")) {
            getSupportActionBar().setTitle(tmpUser.getName() + " (" + tmpUser.getRole() + " of " + tmpUser.getLibraries().get(0).getName() + ")");
            navigation.inflateMenu(R.menu.navigation);
        } else if (tmpUser.getRole().equals("RM")) {
            if (tmpUser.getLibraries().size() > 1) {
                getSupportActionBar().setTitle(tmpUser.getName() + " (" + tmpUser.getRole() + " of " + tmpUser.getLibraries().size() + " Libraries)");
            } else {
                getSupportActionBar().setTitle(tmpUser.getName() + " (" + tmpUser.getRole() + " of " + tmpUser.getLibraries().get(0).getName() + ")");
            }
            navigation.inflateMenu(R.menu.navigation);
        } else {
            getSupportActionBar().setTitle(tmpUser.getName() + " (" + tmpUser.getRole() + ")");
            navigation.inflateMenu(R.menu.navigation_hol);
        }

        navigation.setSelectedItemId(R.id.navigation_home);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onLibraryListFragmentInteraction(Library library) {
        switchToFragment(LibraryReportsFragment.newInstance(library), getString(R.string.FRAGMENT_LIBRARY_REPORTS));
    }

    @Override
    public void onLibraryReportListFragmentInteraction(String libraryName, Report report) {
        String reportTag = null;

        //Report clicked. Find type and transition to show the contents of the Report
        if(report.getType() == ReportType.TYPE_REPORT) {
            // Transition to ReportContentFragment.
            switch (report.getMetric().getType()) {
                case ExtensionMonthly:
                    reportTag = getString(R.string.extension_monthly_report_tag);
                    break;

                case FinanceMonthly:
                    reportTag = getString(R.string.finance_monthly_report_tag);
                    break;

                case FinanceWeekly:
                    reportTag = getString(R.string.finance_weekly_report_tag);
                    break;

                case MeetingsQuarterly:
                    reportTag = getString(R.string.meetings_quarterly_report_tag);
                    break;

                case MeetingsMonthly:
                    reportTag = getString(R.string.meetings_monthly_report_tag);
                    break;

                case MembershipDaily:
                    reportTag = getString(R.string.membership_daily_report_tag);
                    break;

                case ResourcesQuarterly:
                    reportTag = getString(R.string.resources_quarterly_report_tag);
                    break;

                case ResourcesMonthly:
                    reportTag = getString(R.string.resources_monthly_report_tag);
                    break;

                default:
                    Toast.makeText(this, getString(R.string.no_report_choosen), Toast.LENGTH_SHORT).show();
            }

            if (reportTag != null)
                switchToFragment(ReportContentFragment.newInstance(report, libraryName), reportTag);
        }
    }

    @Override
    public void onReportItemClick(Report report) {
        String reportTag = null;

        //Report clicked. Find type and transition to show the contents of the Report
        if(report.getType() == ReportType.TYPE_REPORT) {
            // Transition to ReportContentFragment.
            switch (report.getMetric().getType()) {
                case ExtensionMonthly:
                    reportTag = getString(R.string.extension_monthly_report_tag);
                    break;

                case FinanceMonthly:
                    reportTag = getString(R.string.finance_monthly_report_tag);
                    break;

                case FinanceWeekly:
                    reportTag = getString(R.string.finance_weekly_report_tag);
                    break;

                case MeetingsQuarterly:
                    reportTag = getString(R.string.meetings_quarterly_report_tag);
                    break;

                case MeetingsMonthly:
                    reportTag = getString(R.string.meetings_monthly_report_tag);
                    break;

                case MembershipDaily:
                    reportTag = getString(R.string.membership_daily_report_tag);
                    break;

                case ResourcesQuarterly:
                    reportTag = getString(R.string.resources_quarterly_report_tag);
                    break;

                case ResourcesMonthly:
                    reportTag = getString(R.string.resources_monthly_report_tag);
                    break;

                default:
                    Toast.makeText(this, getString(R.string.no_report_choosen), Toast.LENGTH_SHORT).show();
            }

            if (reportTag != null)
                getReportAndSwitch(report, reportTag);
        }
    }

    public void switchToFragment(Fragment fragment, String tag) {
        fragment = getTheCorrectFragment(fragment);
        FragmentManager manager = getSupportFragmentManager();
        manager.beginTransaction().replace(R.id.baseFrame, fragment, tag).commit();
    }

    public Fragment getTheCorrectFragment (Fragment fragment){
        if (fragment != m_currentFragment && fragment instanceof TopMost){
            m_currentFragment = fragment;
            return ((TopMost) fragment).getTopMostFragment();
        } else if (fragment == m_currentFragment && fragment instanceof TopMost){
            ((TopMost)fragment).clearAll();
        }
        m_currentFragment = fragment;
        return m_currentFragment;
    }

    void getReportAndSwitch(final Report report, final String fragmentTag) {
        String tmpDate = null;
        final String libraryName = tmpUser.getLibraries().get(0).getName();

        if (report.getMetric().getMetricTypeString().matches(".*Daily.*")) {
            tmpDate = CustomDate.getCurrentDate(TimeZone.getTimeZone(this.getApplicationContext().getString(R.string.default_timezone)), CustomDate.CustomDateFormat.Daily);
        } else if (report.getMetric().getMetricTypeString().matches(".*Weekly.*")) {
            tmpDate = CustomDate.getCurrentDate(TimeZone.getTimeZone(this.getApplicationContext().getString(R.string.default_timezone)), CustomDate.CustomDateFormat.Weekly);
        } else if (report.getMetric().getMetricTypeString().matches(".*Monthly.*")) {
            tmpDate = CustomDate.getCurrentDate(TimeZone.getTimeZone(this.getApplicationContext().getString(R.string.default_timezone)), CustomDate.CustomDateFormat.Monthly);
        } else if (report.getMetric().getMetricTypeString().matches(".*Quarterly.*")) {
            tmpDate = CustomDate.getCurrentDate(TimeZone.getTimeZone(this.getApplicationContext().getString(R.string.default_timezone)), CustomDate.CustomDateFormat.Quarterly);
        }

        if (Network.isNetworkAvailable(this.getApplicationContext())) {
            App.mDatabase.child(getString(R.string.FIREBASE_REPORTS)).child(libraryName).child(report.getMetric().getMetricTypeString()).child(tmpDate).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        Report currentReport = dataSnapshot.getValue(Report.class);
                        switchToFragment(ReportContentFragment.newInstance(currentReport, libraryName), fragmentTag);
                    } else {
                        switchToFragment(ReportContentFragment.newInstance(report, libraryName), fragmentTag);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Toast.makeText(getApplicationContext(), getString(R.string.generic_error) + databaseError.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.report_load_no_network), Toast.LENGTH_LONG).show();
            switchToFragment(ReportContentFragment.newInstance(report, libraryName), fragmentTag);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
        // TODO: Home Fragment
    }
}
