package com.ghla.library.authority.utils;

import com.ghla.library.authority.App;
import com.ghla.library.authority.model.Report;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.io.InputStream;

public class JSONHelper {

    public Object deserializeJSONFromFile(String filename, Class c){
        try {
            String json = readJSONFromFile(filename);
            Gson gson = new Gson();
            return gson.fromJson(json,c);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Object deserializeJSON(String json, Class c){
        try {
            Gson gson = new Gson();
            return gson.fromJson(json,c);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String serializeJSON(Object object){
        try {
            Gson gson = new Gson();
            return gson.toJson(object);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    String readJSONFromFile (String filename){
        String json = null;
        try {
            InputStream is = App.getContext().getAssets().open(filename);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return json;
    }

    public String convertReportToJson(Report report){
        Gson gson = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();
        return gson.toJson(report);
    }
}
