package com.ghla.library.authority.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class Subtitle implements Answerable, Parcelable{
    private List<Question> questions;
    private String text;
    private Integer answer;
    private Boolean answerable = false;

    Subtitle() {}

    Subtitle(String text) {
        this.text = text;
    }

    Subtitle (String text, Boolean answerable){
        this.text = text;
        this.answerable = answerable;
    }

    protected Subtitle(Parcel in) {
        questions = in.createTypedArrayList(Question.CREATOR);
        text = in.readString();
        answer = in.readInt();
        answerable = in.readByte() != 0;
    }

    public static final Creator<Subtitle> CREATOR = new Creator<Subtitle>() {
        @Override
        public Subtitle createFromParcel(Parcel in) {
            return new Subtitle(in);
        }

        @Override
        public Subtitle[] newArray(int size) {
            return new Subtitle[size];
        }
    };

    void add (Question question){
        if (this.questions == null){
            this.questions = new ArrayList<>();
        }
        questions.add(question);
    }

    @Override
    public Boolean getAnswerable() {
        return answerable;
    }

    @Override
    public void setAnswerable(Boolean answerable) {
        this.answerable = answerable;
    }

    @Override
    public Integer getAnswer() {
        return answer;
    }

    @Override
    public void setAnswer(Integer answer) {
        this.answer = answer;
    }

    public String getText() {
        return text;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.text);
        if (this.answer != null)
            parcel.writeInt(this.answer);
        if (this.answerable != null)
            parcel.writeByte((byte) (this.answerable ? 1 : 0));
        parcel.writeTypedList(this.questions);
    }
}
