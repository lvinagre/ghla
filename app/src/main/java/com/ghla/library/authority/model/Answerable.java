package com.ghla.library.authority.model;

public interface Answerable{
    Boolean getAnswerable();
    void setAnswerable(Boolean answerable);
    Integer getAnswer();
    void setAnswer(Integer answer);
}
