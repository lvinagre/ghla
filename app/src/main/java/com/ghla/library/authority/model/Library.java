package com.ghla.library.authority.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.database.Exclude;

import java.util.List;

public class Library implements Parcelable{
    private String name;
    private List<Report> reportList;

    //Public methods
    public Library(){}

    public Library(String name, List<Report> reportList) {
        this.name = name;
        this.reportList = reportList;
    }

    protected Library(Parcel in) {
        name = in.readString();
        reportList = in.createTypedArrayList(Report.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeTypedList(reportList);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Library> CREATOR = new Creator<Library>() {
        @Override
        public Library createFromParcel(Parcel in) {
            return new Library(in);
        }

        @Override
        public Library[] newArray(int size) {
            return new Library[size];
        }
    };

    public String getName(){
        return name;
    }

    @Exclude
    public List<Report> getReportList() {
        return reportList;
    }

    public void setReportList(List<Report> reportList) {
        this.reportList = reportList;
    }

    public void addReport (Report report) { this.reportList.add(report); }
}
