package com.ghla.library.authority;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;

import com.ghla.library.authority.model.Library;
import com.ghla.library.authority.model.Report;
import com.ghla.library.authority.utils.CustomDate;
import com.ghla.library.authority.utils.CustomMessage;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import static com.ghla.library.authority.utils.Network.isNetworkAvailable;


public class DashboardFragment extends Fragment {

    private PieChart mChart;
    private Spinner mMetricSpinner, mLibrarySpinner;
    private int lastMetric = 0, lastLibrary = 0;
    private TextView noNetworkTv, noDataTv, mDatePickerFrom, mDatePickerTo;
    private TextView mTableLegendApprovedSummary, mTableLegendSubmittedSummary, mTableLegendChangeRequestedSummary;
    private TextView mTableLegendApprovedSummaryPercentage, mTableLegendSubmittedSummaryPercentage, mTableLegendChangeRequestedSummaryPercentage;
    private ConstraintLayout fullChartLayout;
    private List<Library> rawLibraries = new ArrayList<>();
    private int currYear, currMonth, currDay;
    private String dateFromFilter = "0", dateToFilter = "0";

    public DashboardFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dashboard, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Layout references
        mChart = (PieChart) view.findViewById(R.id.chart);
        mChart.getDescription().setEnabled(false);
        mChart.getLegend().setEnabled(false);
        mChart.setDrawEntryLabels(false);
        mChart.setCenterTextRadiusPercent(98f);
        mChart.setHoleRadius(38f);
        mChart.setTransparentCircleRadius(43f);
        mChart.setCenterText(getString(R.string.dashboard_chart_label));
        mChart.setCenterTextSize(12);
        mChart.setTouchEnabled(false);

        noNetworkTv = (TextView) view.findViewById(R.id.chartNoNetwork);
        noDataTv = (TextView) view.findViewById(R.id.chartNoData);
        fullChartLayout = (ConstraintLayout) view.findViewById(R.id.fullChart);
        mMetricSpinner = (Spinner) view.findViewById(R.id.SpinnerMetrics);
        mLibrarySpinner = (Spinner) view.findViewById(R.id.SpinnerLibrary);
        mDatePickerFrom = (TextView) view.findViewById(R.id.DatePickerFrom);
        mDatePickerTo = (TextView) view.findViewById(R.id.DatePickerTo);
        mTableLegendApprovedSummary = (TextView) view.findViewById(R.id.tableLegendApprovedSummary);
        mTableLegendSubmittedSummary = (TextView) view.findViewById(R.id.tableLegendSubmittedSummary);
        mTableLegendChangeRequestedSummary = (TextView) view.findViewById(R.id.tableLegendChangeRequestedSummary);
        mTableLegendApprovedSummaryPercentage = (TextView) view.findViewById(R.id.tableLegendApprovedSummaryPercentage);
        mTableLegendSubmittedSummaryPercentage = (TextView) view.findViewById(R.id.tableLegendSubmittedSummaryPercentage);
        mTableLegendChangeRequestedSummaryPercentage = (TextView) view.findViewById(R.id.tableLegendChangeRequestedSummaryPercentage);

        currYear = Calendar.getInstance(TimeZone.getTimeZone(getString(R.string.default_timezone))).get(Calendar.YEAR);
        currMonth = Calendar.getInstance(TimeZone.getTimeZone(getString(R.string.default_timezone))).get(Calendar.MONTH);
        currDay = Calendar.getInstance(TimeZone.getTimeZone(getString(R.string.default_timezone))).get(Calendar.DAY_OF_MONTH);

        if (isNetworkAvailable(getContext())) {
            fullChartLayout.setVisibility(View.VISIBLE);
            noNetworkTv.setVisibility(View.GONE);
            rawLibraries.clear();
            loadReports();
        } else {
            if (rawLibraries.size() > 0) {
                fullChartLayout.setVisibility(View.VISIBLE);
                noNetworkTv.setVisibility(View.GONE);
                rawLibraries.clear();
                loadReports();
                CustomMessage.getCustomSnackbar(view, getString(R.string.library_no_network), Snackbar.LENGTH_LONG).show();
            } else {
                fullChartLayout.setVisibility(View.GONE);
                noNetworkTv.setVisibility(View.VISIBLE);
            }
        }

        mMetricSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != lastMetric)
                    lastMetric = position;
                    buildChart();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mLibrarySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != lastLibrary)
                    lastLibrary = position;
                    buildChart();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mDatePickerFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDatePickerFrom.getText().equals(getString(R.string.dashboard_filter_noSelection))) {
                    DatePickerDialog dateFrom = new DatePickerDialog(
                            getContext(),
                            new DatePickerDialog.OnDateSetListener() {
                                @Override
                                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                    try {
                                        String fromDay = (dayOfMonth >= 10) ? String.valueOf(dayOfMonth) : "0" + dayOfMonth;
                                        String fromMonth = (month + 1 >= 10) ? String.valueOf(month + 1) : "0" + (month + 1);
                                        dateFromFilter = String.valueOf(year) + fromMonth + fromDay;
                                        mDatePickerFrom.setText(CustomDate.convertCustomToHuman(getContext(), dateFromFilter, CustomDate.CustomDateFormat.Daily));
                                        buildChart();
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                }
                            },
                            currYear,
                            currMonth,
                            currDay
                    );

                    dateFrom.show();
                } else {
                    mDatePickerFrom.setText(getString(R.string.dashboard_filter_noSelection));
                    dateFromFilter = "0";
                    buildChart();
                }
            }
        });

        mDatePickerTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDatePickerTo.getText().equals(getString(R.string.dashboard_filter_noSelection))) {
                    DatePickerDialog dateTo = new DatePickerDialog(
                            getContext(),
                            new DatePickerDialog.OnDateSetListener() {
                                @Override
                                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                    try {
                                        String toDay = (dayOfMonth >= 10) ? String.valueOf(dayOfMonth) : "0" + dayOfMonth;
                                        String toMonth = (month + 1 >= 10) ? String.valueOf(month + 1) : "0" + (month + 1);
                                        dateToFilter = String.valueOf(year) + toMonth + toDay;
                                        mDatePickerTo.setText(CustomDate.convertCustomToHuman(getContext(), dateToFilter, CustomDate.CustomDateFormat.Daily));
                                        buildChart();
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                }
                            },
                            currYear,
                            currMonth,
                            currDay
                    );

                    dateTo.show();
                } else {
                    mDatePickerTo.setText(getString(R.string.dashboard_filter_noSelection));
                    dateToFilter = "0";
                    buildChart();
                }
            }
        });
    }

    private void loadReports() {
        App.mDatabase.child(getString(R.string.FIREBASE_REPORTS)).addListenerForSingleValueEvent(loadReportsCb);;
    }

    private ValueEventListener loadReportsCb = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            if (dataSnapshot.exists()) {
                for (DataSnapshot library: dataSnapshot.getChildren()) {
                    Library tmpLibrary = new Library(library.getKey(), new ArrayList<Report>());

                    for (DataSnapshot metric: library.getChildren()) {
                        for (DataSnapshot report: metric.getChildren()) {
                            Report tmpReport = report.getValue(Report.class);
                            tmpReport.setDate(report.getKey());
                            tmpLibrary.addReport(tmpReport);
                        }
                    }

                    rawLibraries.add(tmpLibrary);
                }

                fulfilFilters();
                buildChart();
            } else {
                fullChartLayout.setVisibility(View.GONE);
                noDataTv.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
            fullChartLayout.setVisibility(View.GONE);
            noDataTv.setVisibility(View.VISIBLE);
        }
    };

    private void fulfilFilters() {
        String[] metrics = getResources().getStringArray(R.array.report_metrics_array);
        ArrayList<String> metricsList = new ArrayList<>(Arrays.asList(metrics));
        ArrayAdapter<String> metricArrayAdapter = new ArrayAdapter<>(getContext(), R.layout.dashboard_spinner_item, metricsList);
        metricArrayAdapter.insert(getString(R.string.dashboard_filter_noSelection),0);
        mMetricSpinner.setAdapter(metricArrayAdapter);

        List<String> libraryNames = new ArrayList<>();
        for (Library library: rawLibraries) {
            libraryNames.add(library.getName());
        }
        ArrayAdapter<String> libraryArrayAdapter = new ArrayAdapter<>(getContext(), R.layout.dashboard_spinner_item, libraryNames);
        libraryArrayAdapter.insert(getString(R.string.dashboard_filter_noSelection),0);
        mLibrarySpinner.setAdapter(libraryArrayAdapter);
    }

    private void buildChart() {
        // Reset chart table values
        mTableLegendApprovedSummary.setText(getString(R.string.dashboard_calculating));
        mTableLegendSubmittedSummary.setText(getString(R.string.dashboard_calculating));
        mTableLegendChangeRequestedSummary.setText(getString(R.string.dashboard_calculating));
        mTableLegendApprovedSummaryPercentage.setText(getString(R.string.dashboard_calculating));
        mTableLegendSubmittedSummaryPercentage.setText(getString(R.string.dashboard_calculating));
        mTableLegendChangeRequestedSummaryPercentage.setText(getString(R.string.dashboard_calculating));

        // List for each report status
        List<Report> approvedReports = new ArrayList<>(), submittedReports = new ArrayList<>(), changeRequestedReports = new ArrayList<>();

        // Summaries for calculation
        int totalSummary = 0, approvedSummary = 0, submittedSummary = 0, changeRequetedSummary = 0;

        for (Library library: rawLibraries) {
            if (library.getName().equals(mLibrarySpinner.getSelectedItem().toString()) || mLibrarySpinner.getSelectedItem().toString().equals(getString(R.string.dashboard_filter_noSelection))) {
                for (Report report : library.getReportList()) {
                    if (report.getMetric().getMetricTypeString().equals(mMetricSpinner.getSelectedItem().toString()) || mMetricSpinner.getSelectedItem().toString().equals(getString(R.string.dashboard_filter_noSelection))) {
                        if (report.getMetric().getMetricTypeString().matches(".*Daily.*")) {
                            if (Integer.parseInt(report.getDate()) >= Integer.parseInt(dateFromFilter) || dateFromFilter.equals("0")) {
                                if (Integer.parseInt(report.getDate()) <= Integer.parseInt(dateToFilter) || dateToFilter.equals("0")) {
                                    switch (report.getStatus()) {
                                        case Approved:
                                            approvedReports.add(report);
                                            break;
                                        case Submitted:
                                            submittedReports.add(report);
                                            break;
                                        case ChangeRequested:
                                            changeRequestedReports.add(report);
                                            break;
                                    }
                                }
                            }
                        } else if (report.getMetric().getMetricTypeString().matches(".*Weekly.*")) {
                            String tmpDateFromFilter = (dateFromFilter.equals("0")) ? "0" : CustomDate.getDateInCustom(TimeZone.getTimeZone(getString(R.string.default_timezone)), dateFromFilter, CustomDate.CustomDateFormat.Weekly).replace("_", "");
                            String tmpDateToFilter = (dateToFilter.equals("0")) ? "0" : CustomDate.getDateInCustom(TimeZone.getTimeZone(getString(R.string.default_timezone)), dateToFilter, CustomDate.CustomDateFormat.Weekly).replace("_", "");
                            if (Integer.parseInt(report.getDate().replace("_", "")) >= Integer.parseInt(tmpDateFromFilter) || tmpDateFromFilter.equals("0")) {
                                if (Integer.parseInt(report.getDate().replace("_", "")) <= Integer.parseInt(tmpDateToFilter) || tmpDateToFilter.equals("0")) {
                                    switch (report.getStatus()) {
                                        case Approved:
                                            approvedReports.add(report);
                                            break;
                                        case Submitted:
                                            submittedReports.add(report);
                                            break;
                                        case ChangeRequested:
                                            changeRequestedReports.add(report);
                                            break;
                                    }
                                }
                            }
                        } else if (report.getMetric().getMetricTypeString().matches(".*Monthly.*")) {
                            String tmpDateFromFilter = (dateFromFilter.equals("0")) ? "0" : dateFromFilter.substring(0, 6);
                            String tmpDateToFilter = (dateToFilter.equals("0")) ? "0" : dateToFilter.substring(0, 6);
                            if (Integer.parseInt(report.getDate()) >= Integer.parseInt(tmpDateFromFilter) || tmpDateFromFilter.equals("0")) {
                                if (Integer.parseInt(report.getDate()) <= Integer.parseInt(tmpDateToFilter) || tmpDateToFilter.equals("0")) {
                                    switch (report.getStatus()) {
                                        case Approved:
                                            approvedReports.add(report);
                                            break;
                                        case Submitted:
                                            submittedReports.add(report);
                                            break;
                                        case ChangeRequested:
                                            changeRequestedReports.add(report);
                                            break;
                                    }
                                }
                            }
                        } else if (report.getMetric().getMetricTypeString().matches(".*Quarterly.*")) {
                            String tmpDateFromFilter = (dateFromFilter.equals("0")) ? "0" : CustomDate.getDateInCustom(TimeZone.getTimeZone(getString(R.string.default_timezone)), dateFromFilter, CustomDate.CustomDateFormat.Quarterly).replaceAll("_Q", "");
                            String tmpDateToFilter = (dateToFilter.equals("0")) ? "0" : CustomDate.getDateInCustom(TimeZone.getTimeZone(getString(R.string.default_timezone)), dateToFilter, CustomDate.CustomDateFormat.Quarterly).replaceAll("_Q", "");
                            if (Integer.parseInt(report.getDate().replaceAll("_Q", "")) >= Integer.parseInt(tmpDateFromFilter) || tmpDateFromFilter.equals("0")) {
                                if (Integer.parseInt(report.getDate().replaceAll("_Q", "")) <= Integer.parseInt(tmpDateToFilter) || tmpDateToFilter.equals("0")) {
                                    switch (report.getStatus()) {
                                        case Approved:
                                            approvedReports.add(report);
                                            break;
                                        case Submitted:
                                            submittedReports.add(report);
                                            break;
                                        case ChangeRequested:
                                            changeRequestedReports.add(report);
                                            break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        PieDataSet set = new PieDataSet(null, null);
        set.setDrawValues(false);
        set.resetColors();

        if (approvedReports.size() > 0) {
            for (Report report: approvedReports) {
                approvedSummary += report.getReportSummary();
            }
            set.addEntry(new PieEntry(approvedSummary, getString(R.string.dashboard_report_approved)));
            set.addColor(ContextCompat.getColor(getContext(), R.color.colorGreen));
            mTableLegendApprovedSummary.setText(String.valueOf(approvedSummary));
            totalSummary += approvedSummary;
        } else {
            mTableLegendApprovedSummary.setText("0");
        }

        if (submittedReports.size() > 0) {
            for (Report report: submittedReports) {
                submittedSummary += report.getReportSummary();
            }
            set.addEntry(new PieEntry(submittedSummary, getString(R.string.dashboard_report_submitted)));
            set.addColor(ContextCompat.getColor(getContext(), R.color.colorYellow));
            mTableLegendSubmittedSummary.setText(String.valueOf(submittedSummary));
            totalSummary += submittedSummary;
        } else {
            mTableLegendSubmittedSummary.setText("0");
        }

        if (changeRequestedReports.size() > 0) {
            for (Report report: changeRequestedReports) {
                changeRequetedSummary += report.getReportSummary();
            }
            set.addEntry(new PieEntry(changeRequetedSummary, getString(R.string.dashboard_report_changeRequested)));
            set.addColor(ContextCompat.getColor(getContext(), R.color.colorBlue));
            mTableLegendChangeRequestedSummary.setText(String.valueOf(changeRequetedSummary));
            totalSummary += changeRequetedSummary;
        } else {
            mTableLegendChangeRequestedSummary.setText("0");
        }

        if (set.getEntryCount() > 0) {
            PieData data = new PieData(set);
            data.setValueTextColor(ContextCompat.getColor(getContext(), R.color.colorWhite));
            data.setValueTextSize(16);
            mChart.setData(data);
        } else {
            mChart.setData(null);
        }

        // Calculate percentages
        if (totalSummary > 0) {
            mTableLegendApprovedSummaryPercentage.setText(String.format("%.2f %%", (float) approvedSummary / totalSummary * 100));
            mTableLegendSubmittedSummaryPercentage.setText(String.format("%.2f %%", (float) submittedSummary / totalSummary * 100));
            mTableLegendChangeRequestedSummaryPercentage.setText(String.format("%.2f %%", (float) changeRequetedSummary / totalSummary * 100));
        } else {
            mTableLegendApprovedSummaryPercentage.setText("0.00 %");
            mTableLegendSubmittedSummaryPercentage.setText("0.00 %");
            mTableLegendChangeRequestedSummaryPercentage.setText("0.00 %");
        }

        mChart.invalidate();
    }
}
