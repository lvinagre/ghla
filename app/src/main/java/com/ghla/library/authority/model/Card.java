package com.ghla.library.authority.model;
public class Card {
    private String m_title;
    private int m_image;
    private int m_color;

    public Card(String title, int image, int color) {
        this.m_title = title;
        this.m_image = image;
        this.m_color = color;
    }

    public String getTitle() {
        return m_title;
    }
    public int getImage() {
        return m_image;
    }
    public int getColor(){ return m_color; }
}