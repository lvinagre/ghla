package com.ghla.library.authority;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.ghla.library.authority.utils.CustomMessage;
import com.ghla.library.authority.utils.SharedPrefs;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;


/**
 * A simple {@link Fragment} subclass.
 */
public class EmailFragment extends Fragment {

    Fragment emailFragment = this;
    private ProfileFragment m_profileFragment;

    // Layout references variables
    private TextView mCurrentEmail;
    private Button submitButton, cancelButton;
    private EditText emailEditText;

    // Firebase auth reference
    private FirebaseAuth mAuth;

    public EmailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Instantiate firebase
        mAuth = FirebaseAuth.getInstance();

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_email, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Layout references
        submitButton = (Button) view.findViewById(R.id.email_change_submit_button);
        cancelButton = (Button) view.findViewById(R.id.email_change_cancel_button);
        emailEditText = (EditText) view.findViewById(R.id.change_email_value);
        mCurrentEmail = (TextView) view.findViewById(R.id.current_email_value);

        // Set current e-mail to layout
        mCurrentEmail.setText(mAuth.getCurrentUser().getEmail());

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (emailEditText.getText().toString().matches(".+@.+(\\.[a-zA-Z]{2,3}){1,2}")) {
                    submitNewEmail(emailEditText.getText().toString());
                } else {
                    emailEditText.setError(getString(R.string.error_invalid_email));
                }
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismissFragment();
            }
        });
    }

    private void submitNewEmail(String email) {
        submitButton.setVisibility(View.INVISIBLE);
        cancelButton.setVisibility(View.INVISIBLE);
        mAuth.getCurrentUser().updateEmail(email)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            CustomMessage.getCustomSnackbar(getView(),R.string.fragment_email_ok, Snackbar.LENGTH_SHORT).addCallback(new Snackbar.Callback() {
                                @Override
                                public void onDismissed(Snackbar snackbar, int event) {
                                    mAuth.signOut();
                                    SharedPrefs.removeUser(emailFragment.getActivity());
                                    Intent intent = new Intent(getView().getContext(), LoginActivity.class);
                                    startActivity(intent);
                                    emailFragment.getActivity().finish();
                                }
                            }).show();
                        } else {
                            CustomMessage.getCustomSnackbar(getView(),getString(R.string.generic_error) + " " + task.getException().getMessage(), Snackbar.LENGTH_LONG ).show();

                            submitButton.setVisibility(View.VISIBLE);
                            cancelButton.setVisibility(View.VISIBLE);
                        }
                    }
                });
    }

    private void dismissFragment(){
        m_profileFragment = m_profileFragment == null ? new ProfileFragment() : m_profileFragment;
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(emailFragment.getId(), m_profileFragment, getString(R.string.FRAGMENT_PROFILE));
        fragmentTransaction.commit();
    }
}
