package com.ghla.library.authority.utils;

import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.TextView;

import com.ghla.library.authority.R;

public class CustomMessage {

    public static Snackbar getCustomSnackbar(View view, String message, int duration) {
        final Snackbar snackbar = Snackbar.make(view,message, duration);

        snackbar.setActionTextColor(view.getResources().getColor(R.color.colorWhite));
        ((TextView) snackbar.getView().findViewById(android.support.design.R.id.snackbar_text)).setMaxLines(3);

        snackbar.setAction("X", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                snackbar.dismiss();
            }
        });

        return snackbar;
    }

    public static Snackbar getCustomSnackbar(View view, int resId, int duration) {
        final Snackbar snackbar = Snackbar.make(view, view.getResources().getString(resId), duration);

        snackbar.setActionTextColor(view.getResources().getColor(R.color.colorWhite));
        ((TextView) snackbar.getView().findViewById(android.support.design.R.id.snackbar_text)).setMaxLines(3);

        snackbar.setAction("X", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                snackbar.dismiss();
            }
        });

        return snackbar;
    }
}
