package com.ghla.library.authority.model;

import java.util.List;

public class User {
    private String name;
    private String role;
    private List<Library> libraries;

    // Public methods
    public User(){}

    public User(String name, String role, List<Library> libraries) {
        this.name = name;
        this.role = role;
        this.libraries = libraries;
    }

    public String getName(){
        return this.name;
    }
    public String getRole() { return this.role; }

    public List<Library> getLibraries() {
        return libraries;
    }

    public void setLibraries(List<Library> libraries) {
        this.libraries = libraries;
    }
}
