package com.ghla.library.authority;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ghla.library.authority.utils.CustomMessage;
import com.ghla.library.authority.utils.SharedPrefs;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class ProfileFragment extends Fragment {

    private EmailFragment m_emailFragment;
    private Fragment profileFragment = this;

    // Firebase auth reference
    private FirebaseAuth mAuth;

    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Instantiate firebase
        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupMainViews();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void setupMainViews() {
        View emailView = getView().findViewById(R.id.card_email);
        setTitleAndSubTitle(emailView, getString(R.string.change_email_title), getString(R.string.change_email_subtitle));
        emailView.setOnClickListener(emailListener);

        View passwordView = getView().findViewById(R.id.card_password);
        setTitleAndSubTitle(passwordView, getString(R.string.change_password_title), getString(R.string.change_password_subtitle));
        passwordView.setOnClickListener(passwordListener);

        View logoutView = getView().findViewById(R.id.card_logout);
        setTitleAndSubTitle(logoutView, getString(R.string.logout_title), getString(R.string.logout_subtitle));
        logoutView.setOnClickListener(logoutListener);
    }

    private void setTitleAndSubTitle(View view, String title, String subtitle) {
        TextView titleView = view.findViewById(R.id.title);
        TextView subtitleView = view.findViewById(R.id.subtitle);
        titleView.setText(title);
        subtitleView.setText(subtitle);
    }

    View.OnClickListener emailListener = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            m_emailFragment = m_emailFragment == null ? new EmailFragment() : m_emailFragment;
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(profileFragment.getId(), m_emailFragment, getString(R.string.FRAGMENT_EMAIL));
            fragmentTransaction.commit();
        }
    };

    View.OnClickListener passwordListener = new View.OnClickListener(){
        @Override
        public void onClick(final View v) {
            mAuth.sendPasswordResetEmail(mAuth.getCurrentUser().getEmail())
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                CustomMessage.getCustomSnackbar(v,R.string.change_password_ok, Snackbar.LENGTH_SHORT ).addCallback(new Snackbar.Callback() {
                                    @Override
                                    public void onDismissed(Snackbar snackbar, int event) {
                                        logoutUser(v);
                                    }
                                }).show();
                            } else {
                                CustomMessage.getCustomSnackbar(v,R.string.change_password_error, Snackbar.LENGTH_SHORT ).show();
                            }
                        }
                    });
        }
    };

    View.OnClickListener logoutListener = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            logoutUser(v);
        }
    };

    private void logoutUser(View v) {
        mAuth.signOut();
        SharedPrefs.removeUser(this.getActivity());
        Intent intent = new Intent(v.getContext(), LoginActivity.class);
        startActivity(intent);
        getActivity().finish();
    }
}
