package com.ghla.library.authority.utils;

import android.app.Activity;

import com.ghla.library.authority.model.Metric;
import com.ghla.library.authority.model.Report;
import com.ghla.library.authority.model.ReportHeader;

import java.util.ArrayList;

public class ReportsGenerator {

    // Daily
    private static final String MEMBERSHIP_DAILY_JSON = "membership_daily.json";

    // Weekly
    private static final String FINANCE_WEEKLY_JSON = "finance_weekly.json";

    // Monthly
    private static final String EXTENSION_MONTHLY_JSON = "extension_monthly.json";
    private static final String FINANCE_MONTHLY_JSON = "finance_monthly.json";
    private static final String MEETINGS_MONTHLY_JSON = "meetings_monthly.json";
    private static final String RESOURCES_MONTHLY_JSON = "resources_monthly.json";

    // Quarterly
    private static final String MEETINGS_QUARTERLY_JSON = "meetings_quarterly.json";
    private static final String RESOURCES_QUARTERLY_JSON = "resources_quarterly.json";

    public ArrayList<Report> getReports(Activity activity){
        ArrayList<Report> reports = new ArrayList<>();

        // Daily
        reports.add(new ReportHeader(ReportHeader.Header.Daily));
        reports.add(generateReport(Metric.MetricType.MembershipDaily, MEMBERSHIP_DAILY_JSON, Report.Status.NotStarted));

        // Weekly
        reports.add(new ReportHeader(ReportHeader.Header.Weekly));
        reports.add(generateReport(Metric.MetricType.FinanceWeekly, FINANCE_WEEKLY_JSON, Report.Status.NotStarted));

        // Monthly
        reports.add(new ReportHeader(ReportHeader.Header.Monthly));
        reports.add(generateReport(Metric.MetricType.ExtensionMonthly, EXTENSION_MONTHLY_JSON, Report.Status.NotStarted));
        reports.add(generateReport(Metric.MetricType.FinanceMonthly, FINANCE_MONTHLY_JSON, Report.Status.NotStarted));
        reports.add(generateReport(Metric.MetricType.MeetingsMonthly, MEETINGS_MONTHLY_JSON, Report.Status.NotStarted));
        reports.add(generateReport(Metric.MetricType.ResourcesMonthly, RESOURCES_MONTHLY_JSON, Report.Status.NotStarted));

        // Quarterly
        reports.add(new ReportHeader(ReportHeader.Header.Quarterly));
        reports.add(generateReport(Metric.MetricType.MeetingsQuarterly, MEETINGS_QUARTERLY_JSON, Report.Status.NotStarted));
        reports.add(generateReport(Metric.MetricType.ResourcesQuarterly, RESOURCES_QUARTERLY_JSON, Report.Status.NotStarted));

        return reports;
    }

    public Report generateReport(Metric.MetricType metric, String jsonQuestionnaire, Report.Status status) {
        Report report = (Report) new JSONHelper().deserializeJSONFromFile(jsonQuestionnaire,Report.class);
        report.setMetric(new Metric(metric));
        report.setStatus(status);
        return report;
    }
}
