package com.ghla.library.authority;

import android.app.Application;
import android.content.Context;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class App extends Application {

    private static Context m_context;
    public  static DatabaseReference mDatabase;

    @Override
    public void onCreate() {
        super.onCreate();
        m_context = getApplicationContext();

        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        FirebaseDatabase.getInstance().getReference().keepSynced(true);
        mDatabase = FirebaseDatabase.getInstance().getReference();
    }

    public static Context getContext() {
        return m_context;
    }
}
