package com.ghla.library.authority.model;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;

import com.ghla.library.authority.R;
import com.google.firebase.database.Exclude;

import java.util.ArrayList;
import java.util.List;

public class Report implements ReportType, Parcelable{

    // Also used to build dashboard
    public enum Status{
        NotStarted, InProgress, Submitted, Approved, ChangeRequested
    }

    private Metric metric;
    private Status status;
    private List<Title> report;
    private String date;

    //Default constructor for the subclasses
    Report(){}

    public Report(Metric mMetric, List<Title> reportContent, String date) {
        this.metric = mMetric;
        this.status = Status.NotStarted;
        this.report = reportContent;
        this.date = date;
    }

    // Public methods
    public Metric getMetric() {
        return metric;
    }

    public void setMetric(Metric metric) {
        this.metric = metric;
    }

    public void setReport(List<Title> reportContent) {
        this.report = reportContent;
    }

    public List<Title> getReport() {
        return report;
    }

    public void setStatus(Status mStatus) {
        this.status = mStatus;
    }

    public Status getStatus() {
        return status;
    }

    @Exclude
    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getColor(Context context) {
        switch (this.metric.getType()) {
            case ExtensionMonthly:
                return ContextCompat.getColor(context, R.color.colorCardOrange);
            case FinanceWeekly:
            case FinanceMonthly:
                return ContextCompat.getColor(context, R.color.colorCardPink);
            case MeetingsQuarterly:
            case MeetingsMonthly:
                return ContextCompat.getColor(context, R.color.colorCardPaleGreen);
            case MembershipDaily:
                return ContextCompat.getColor(context, R.color.colorCardYellow);
            case ResourcesQuarterly:
            case ResourcesMonthly:
                return ContextCompat.getColor(context, R.color.colorCardGreen);
            default:
                return ContextCompat.getColor(context, R.color.colorCardYellow);
        }
    }

    public int getImage(Context context){
        switch (this.metric.getType()) {
            case ExtensionMonthly:
                return R.drawable.ic_extension;
            case FinanceWeekly:
            case FinanceMonthly:
                return R.drawable.ic_finance;
            case MeetingsQuarterly:
            case MeetingsMonthly:
                return R.drawable.ic_meetings;
            case MembershipDaily:
                return R.drawable.ic_membership;
            case ResourcesQuarterly:
            case ResourcesMonthly:
                return R.drawable.ic_resources;
            default:
                return R.drawable.ic_membership;
        }
    }

    //Return the sum of report answers values
    public int getReportSummary() {
        int sum = 0;

        for (Title title: report) {
            if (title.getSubtitles() != null){
                for (Subtitle subtitle : title.getSubtitles()) {
                    if (subtitle.getQuestions() != null) {
                        for (Question question : subtitle.getQuestions()) {
                            sum += question.getAnswer();
                        }
                    } else {
                        sum += subtitle.getAnswer();
                    }
                }
            } else {
                sum += title.getAnswer();
            }
        }

        return sum;
    }

    protected Report(Parcel in) {
        int tmpMType = in.readInt();
        this.metric.setType(tmpMType == -1 ? null : Metric.MetricType.values()[tmpMType]);
        this.report = new ArrayList<Title>();
        in.readList(this.report, Title.class.getClassLoader());
        this.date = in.readString();
    }

    public static final Creator<Report> CREATOR = new Creator<Report>() {
        @Override
        public Report createFromParcel(Parcel source) {
            return new Report(source);
        }

        @Override
        public Report[] newArray(int size) {
            return new Report[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.metric.getType() == null ? -1 : this.metric.getType().ordinal());
        dest.writeInt((this.status == null ? -1 : this.status.ordinal()));
        dest.writeList(this.report);
        dest.writeString(this.date);
    }

    @Override
    @Exclude
    public int getType() {
        return TYPE_REPORT;
    }
}


