package com.ghla.library.authority.model;

public interface ReportType {
    int TYPE_HEADER = 0;
    int TYPE_REPORT = 1;

    int getType();
}
