package com.ghla.library.authority;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ghla.library.authority.utils.ReportsGenerator;

import static com.ghla.library.authority.model.ReportType.TYPE_HEADER;

public class ReportsFragment extends Fragment {

    private static final String ARG_COLUMN_COUNT = "column-count";
    private int mColumnCount = 2;
    private ReportsCardViewAdapter.OnItemClickListener mListener;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ReportsFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reports_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            GridLayoutManager glm = new GridLayoutManager(context, mColumnCount);
            final ReportsCardViewAdapter reportsCardViewAdapter = new ReportsCardViewAdapter (new ReportsGenerator().getReports(this.getActivity()), mListener);
            glm.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup(){
                @Override
                public int getSpanSize(int position) {
                    switch(reportsCardViewAdapter.getItemViewType(position)) {
                        case TYPE_HEADER:
                            return 2;
                        default:
                            return 1;
                    }
                }
            });
            recyclerView.setLayoutManager(glm);
            recyclerView.setAdapter(reportsCardViewAdapter);
        }
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ReportsCardViewAdapter.OnItemClickListener) {
            mListener = (ReportsCardViewAdapter.OnItemClickListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
}
