package com.ghla.library.authority;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ghla.library.authority.LibraryFragment.OnListFragmentInteractionListener;
import com.ghla.library.authority.model.Library;
import com.ghla.library.authority.model.Report;
import com.ghla.library.authority.utils.CustomDate;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

public class MyLibraryRecyclerViewAdapter extends RecyclerView.Adapter<MyLibraryRecyclerViewAdapter.ViewHolder> {

    private final List<Library> mLibraries;
    private final OnListFragmentInteractionListener mListener;
    private Context mContext;

    public MyLibraryRecyclerViewAdapter(List<Library> libraries, OnListFragmentInteractionListener listener) {
        mLibraries = libraries;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.mContext = parent.getContext();
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_library, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mLibrary = mLibraries.get(position);
        Library library = mLibraries.get(position);
        holder.vLayout.setBackgroundColor(getRandomLibraryColor());
        holder.vName.setText(library.getName());
        getSubmittedReports(library, holder);
    }

    @Override
    public int getItemCount() {
        return mLibraries.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        protected ConstraintLayout vLayout;
        protected  TextView vName, vBadge;
        public Library mLibrary;

        public ViewHolder(View view) {
            super(view);
            vLayout = view.findViewById(R.id.library_layout);
            vName = view.findViewById(R.id.library_name);
            vBadge = view.findViewById(R.id.library_card_badge);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + vName.getText() + "'";
        }
    }

    static int currentLibraryColor = 0;
    int getRandomLibraryColor(){
        int[] androidColors = mContext.getResources().getIntArray(R.array.libraryColors);
        return androidColors[currentLibraryColor++%androidColors.length];
    }


    private void getSubmittedReports(final Library library, final ViewHolder holder) {
        App.mDatabase.child(mContext.getString(R.string.FIREBASE_REPORTS)).child(library.getName()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    library.setReportList(new ArrayList<Report>());
                    for (DataSnapshot metric: dataSnapshot.getChildren()) {
                        for (DataSnapshot report: metric.getChildren()) {
                            Report tmpReport = report.getValue(Report.class);
                            tmpReport.setDate(report.getKey());

                            // Check if it is submitted
                            if (tmpReport.getStatus() == Report.Status.Submitted) {
                                if (metric.getKey().matches("Daily.*")) {
                                    if (Integer.parseInt(report.getKey()) < Integer.parseInt(CustomDate.getCurrentDate(TimeZone.getTimeZone(mContext.getString(R.string.default_timezone)), CustomDate.CustomDateFormat.Daily)))
                                        library.addReport(tmpReport);
                                } else if (metric.getKey().matches("Weekly.*")) {
                                    int reportYear = Integer.parseInt(report.getKey().split("_")[0]);
                                    int reportWeek = Integer.parseInt(report.getKey().split("_")[1]);
                                    int currentYear = Integer.parseInt(CustomDate.getCurrentDate(TimeZone.getTimeZone(mContext.getString(R.string.default_timezone)), CustomDate.CustomDateFormat.Weekly).split("_")[0]);
                                    int currentWeek = Integer.parseInt(CustomDate.getCurrentDate(TimeZone.getTimeZone(mContext.getString(R.string.default_timezone)), CustomDate.CustomDateFormat.Weekly).split("_")[1]);
                                    if (reportYear < currentYear) {
                                        library.addReport(tmpReport);
                                    } else if (reportYear == currentYear) {
                                        if (reportWeek < currentWeek)
                                            library.addReport(tmpReport);
                                    }
                                } else if (metric.getKey().matches("Monthly.*")) {
                                    if (Integer.parseInt(report.getKey()) < Integer.parseInt(CustomDate.getCurrentDate(TimeZone.getTimeZone(mContext.getString(R.string.default_timezone)), CustomDate.CustomDateFormat.Monthly)))
                                        library.addReport(tmpReport);
                                } else if (metric.getKey().matches("Quarterly.*")) {
                                    int reportYear = Integer.parseInt(report.getKey().split("_Q")[0]);
                                    int reportQuarter = Integer.parseInt(report.getKey().split("_Q")[1]);
                                    int currentYear = Integer.parseInt(CustomDate.getCurrentDate(TimeZone.getTimeZone(mContext.getString(R.string.default_timezone)), CustomDate.CustomDateFormat.Quarterly).split("_Q")[0]);
                                    int currentQuarter = Integer.parseInt(CustomDate.getCurrentDate(TimeZone.getTimeZone(mContext.getString(R.string.default_timezone)), CustomDate.CustomDateFormat.Quarterly).split("_Q")[1]);
                                    if (reportYear < currentYear) {
                                        library.addReport(tmpReport);
                                    } else if (reportYear == currentYear) {
                                        if (reportQuarter < currentQuarter)
                                            library.addReport(tmpReport);
                                    }
                                }
                            }
                        }
                    }

                    if (library.getReportList().size() > 0) {
                        if (library.getReportList().size() <= 99) {
                            holder.vBadge.setText(String.valueOf(library.getReportList().size()));
                        } else {
                            holder.vBadge.setText("+");
                        }
                        holder.vBadge.setVisibility(View.VISIBLE);

                        holder.itemView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (null != mListener) {
                                    // Notify the active callbacks interface (the activity, if the
                                    // fragment is attached to one) that an item has been selected.
                                    mListener.onLibraryListFragmentInteraction(holder.mLibrary);
                                }
                            }
                        });
                    } else {
                        holder.itemView.setOnClickListener(null);
                        holder.vBadge.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
