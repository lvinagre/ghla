package com.ghla.library.authority.model;

public class ReportHeader extends Report implements ReportType {

    Header mValue;

    public enum Header {
        Daily, Weekly, Monthly, Quarterly
    }

    public ReportHeader(Header header){
        this.mValue = header;
    }

    @Override
    public int getType() {
        return TYPE_HEADER;
    }

    @Override
    public String toString() {
        switch (mValue) {
            case Daily:
                return "due today";
            case Weekly:
                return "due this week";
            case Monthly:
                return "due this month";
            case Quarterly:
                return "due this quarter";
            default:
                return "due soon";
        }
    }
}
