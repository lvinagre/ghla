package com.ghla.library.authority.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Question implements Answerable, Parcelable{
    private String text;
    private Integer answer;
    private Boolean answerable = false;

    public Question() {}

    public Question(String questionText) {
        this.text = questionText;
    }

    public Question (String text, Boolean answerable){
        this.text = text;
        this.answerable = answerable;
    }

    protected Question(Parcel in) {
        text = in.readString();
        answer = in.readInt();
        answerable = in.readByte() != 0;
    }

    public static final Creator<Question> CREATOR = new Creator<Question>() {
        @Override
        public Question createFromParcel(Parcel in) {
            return new Question(in);
        }

        @Override
        public Question[] newArray(int size) {
            return new Question[size];
        }
    };

    public String getText() {
        return this.text;
    }

    public void setText (String text) { this.text = text;}

    @Override
    public Boolean getAnswerable() {
        return answerable;
    }

    @Override
    public void setAnswerable(Boolean answerable) {
        this.answerable = answerable;
    }

    @Override
    public Integer getAnswer() {
        return answer;
    }

    @Override
    public void setAnswer(Integer answer) {
        this.answer = answer;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.text);
        if (this.answer != null)
            parcel.writeInt(this.answer);
        if (this.answerable != null)
            parcel.writeByte((byte) (this.answerable ? 1 : 0));
    }
}
