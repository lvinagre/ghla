package com.ghla.library.authority.model;

import com.google.firebase.database.Exclude;

public class Metric{
    private MetricType type;

    // Metrics list
    // * Must update array resource if add/delete any *
    public static final String RESOURCESQUARTERLY = "Quarterly Resources";
    public static final String RESOURCESMONTHLY = "Monthly Resources";
    public static final String FINANCEMONTHLY = "Monthly Finance";
    public static final String FINANCEWEEKLY = "Weekly Finance";
    public static final String MEMBERSHIPDAILY = "Daily Membership";
    public static final String EXTENSIONMONTHLY = "Monthly Extension";
    public static final String MEETINGSMONTHLY = "Monthly Meetings";
    public static final String MEETINGSQUARTERLY = "Quarterly Meetings";
    public static final String HEADER = "Header";

    public enum MetricType{
        MembershipDaily, ResourcesQuarterly, ResourcesMonthly, ExtensionMonthly, FinanceMonthly, FinanceWeekly, MeetingsMonthly, MeetingsQuarterly, Header
    }

    @Exclude
    public String getMetricTypeString(){
        switch (type){
            case ResourcesQuarterly:
                return RESOURCESQUARTERLY;
            case ResourcesMonthly:
                return RESOURCESMONTHLY;
            case FinanceMonthly:
                return FINANCEMONTHLY;
            case FinanceWeekly:
                return FINANCEWEEKLY;
            case MembershipDaily:
                return MEMBERSHIPDAILY;
            case ExtensionMonthly:
                return EXTENSIONMONTHLY;
            case MeetingsMonthly:
                return MEETINGSMONTHLY;
            case MeetingsQuarterly:
                return MEETINGSQUARTERLY;
            default:
                return HEADER;
        }
    }

    public Metric() {}

    public Metric(MetricType type) {
        this.type = type;
    }

    public MetricType getType() {
        return type;
    }

    public void setType(MetricType type) {
        this.type = type;
    }
}
