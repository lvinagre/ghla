# Android app for Ghla (Ghana Library Authority)

This app contains the implementation of the Login, Home Page, Profile Page and the Reports Page(Membership Metric alone completed as of June 14, 2018).

As of v1.1, the Login, Profile, Reports and Dashboad views have been integrated with Firebase and it is now current using the Firebase Authentication and Realtime Database modules.


## Getting Started

Before looking into the app, it's important to understand the KPI metrics, different Users (Manager, Regional Manager and Head of Libraries), different question types (Title, Subtitle and Question). The KPI metrics and question types are defined in the Excel Sheet (not shared here) and the different User roles are defined in the initial ramp up document. Contact the project manager for the most recent information and for the Excel sheet.


## Workflow (Non Technical Overview)

### User 1: Manager
When a Manager logs into the app, they see a list of different metrics that need reporting (as described in the above-mentioned excel sheet). The user chooses one of the metrics and starts filling in the values for the different fields in that metric.

Once the user fills in that information and submits the report, it is saved on the DB and its status changed.


### User 2: Regional Manager

When a Regional Manager logs into the app, they see a list of reports submitted by the Managers. They can then go over the reports and Approve them or Request for change.


### User 3: Head of libraries

The Head of the libraries gets an birds eye view of all the libraries. They get a cumulative status of all the reports and they can filter by libraries and/or metrics and/or date.


## Data architecture and Components

### Hybrid Approach

The key part of the data architecture is the KPI metrics. The KPI metrics which were originally in the Excel sheet has been translated to JSON format. JSON is chosen as the data format because it is human readable + machine readable and light weight. The app uses a hybrid approach for the KPI metrics. It keeps a copy of the metric locally and also in the server. Since the app needs to be fast and light weight, and the KPI metrics don't change frequently, the KPI metrics are cached in the app. The cached version is used when there is no internet or when there is no data saved in Firebase.

The code is flexible enough such that when a metric is edited in the server/client, the Android UI components dynamically add EditText views accordingly based on the JSON file. The process is explained in the *Editing the JSON* section.

### Editing the JSON

* Before editing

```
       { "text": "JUVENILE",
          "questions":[
            {"text": "Male","answerable":true},
            {"text": "Female","answerable":true}
          ]
       }
```
The column above with the text JUVENILE is not editable. Let's assume that we want to make the users enter value next to the JUVENILE column. All we have to do is add "answerable":"true" right next to it, as shown below.

* After editing

```
      { "text": "JUVENILE", "answerable":"true"
          "questions":[
            {"text": "Male","answerable":true},
            {"text": "Female","answerable":true}
          ]
      }
```

No other code changes are needed within the app. The app is flexible enough to render without any compilation errors. If you encounter any errors here, make sure that there are no errors in the JSON and also make sure that JSON keys comply the data model types defined in the app. Gson is used to serialize and deserialize the JSON .

### Classes and Interfaces

Important classes and their functions are listed here.

Title, Subtitle and Question are the main objects that are deserialized from the JSON. All these classes implement the Answerable interface to wire the data up with the UI components.

JSONHelper provides the necessary abstraction for  taking care of serializing and deserializing with the Gson library. If Gson were to be replaced by another library, then code changes can be limited only to this file.

Answerable interface is important for the UI components to work seamlessly.

The different question types (Title, subtitle and Question) implement the Answerable interface. If a new question type were to be added in the future, it's important that the new question type implements the Answerable interface for the UI components to work seamlessly.


## Authors

* *Initial work* - [ **Sundar Venkatesh**](https://github.com/maniksundar)
* *Login, reports flow and dashboard using Firebase* - [**Leonardo Vinagre**](https://gitlab.com/lvinagre)


## License

This project is licensed under the MIT License.


## Acknowledgments

* This project is part of the online volunteer project to United Nations @ https://www.onlinevolunteering.org/en
